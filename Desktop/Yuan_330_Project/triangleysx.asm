;Songxiang Yuan
;13 DEC 2016
;CSC-330 final project

;define constant
.def	workhorse		= r16
.def	amplitude		= r17
.def	update			= r18
.def	dir				= r19
;set up interrupt and setup
.cseg
.org	0x0000
		rjmp		setup
.org	0x0020
		rjmp		ISR_OV0
.org	0x002A
		rjmp		ISR_1
.org	0x0100
;the basic setup
setup:	
		ser			workhorse					;set all bit in workhorse to 1
		out			DDRD,	workhorse			;
		ldi			workhorse,	0b00000000		
		out			TCCR0A,	workhorse			;load TCCR0A with 0b00000000
		ldi			workhorse,	0b00000101		 
		out			TCCR0B,	workhorse			;load TCCR0B with 0b00000101
		ldi			workhorse,	0b01100101		;load ADMUX with A5.
		sts			ADMUX,	workhorse
		ldi			workhorse,	0b11101111		;load ADCSRA so that enable the input.
		sts			ADCSRA,	workhorse
		ldi			workhorse,	0b00000001
		sts			TIMSK0,	workhorse			;Overflow Interrupt Enable
		ldi			dir,1	
		sei

;pick the direction, up or down
tri_dir:
		cpi			dir, 1
		breq		tri_up
		rjmp		tri_dn			
;the up loop
tri_up:	
		ldi			dir, 1
		inc			amplitude	
		cpi			amplitude	, 255
		breq		tri_to0
		rjmp		tri_update
;the down loop
tri_dn:	
		ldi			dir, 0
		dec			amplitude	
		cpi			amplitude	, 0
		breq		tri_to1
		rjmp		tri_update		
;change dirction to up
tri_to1:
		ldi			dir,1
		rjmp		tri_up
;change dirction to down
tri_to0:
		ldi			dir,0
		rjmp		tri_dn
;change the update to 0
tri_update:
		ldi			update, 0
		rjmp		main_loop		
;the main loop to output
main_loop:	
		out			PORTD, amplitude						
		cpi			update, 1					
		breq		tri_dir
		rjmp		main_loop
;timer interrput
ISR_OV0:
		out			TCNT0, workhorse
		ldi			update, 1
		reti

;ADC interrput
ISR_1:
		lds			workhorse, ADCH

		reti