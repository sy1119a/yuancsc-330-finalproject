;Songxiang Yuan
;13 DEC 2016
;CSC-330 final project

;define constant
.def	workhorse		= r16
.def	amplitude		= r17
;set up interrupt and setup
.cseg
.org	0x0000
		rjmp		setup
.org	0x0020
		rjmp		ISR0
.org	0x002A
		rjmp		ISR1
.org	0x0100
;the basic setup
setup:	
		ser			workhorse
		out			DDRD,	workhorse
		ldi			workhorse, 0b00000000
		out			TCCR0A,	workhorse
		ldi			workhorse, 0b00000101
		out			TCCR0B,	workhorse
		ldi			workhorse, 0b01100101	;load ADMUX with A5.
		sts			ADMUX,	workhorse
		ldi			workhorse, 0b11101111	;load ADCSRA so that enable the input.
		sts			ADCSRA,	workhorse
		ldi			workhorse, 0b00000001
		sts			TIMSK0,	workhorse			;Timer/Counter0, Overflow Interrupt Enable
		sei
;loop to output
loop:	out			PORTD, amplitude		;output to portD
		rjmp		loop


;timer interrput
ISR0:
		inc			amplitude				;
		out			TCNT0, workhorse			;load timer with preload
		reti

;ADC interrput
ISR1:
		lds			workhorset, ADCH			;load preload with ADC value
		reti