.def	loader			= r16
.def	amplitude		= r17
.cseg
.org	0x0000
		rjmp		setup
.org	0x0020
		rjmp		ISR_OV0
.org	0x0100

setup:	
		ser			loader
		out			DDRD, loader
		ldi			workhorse,	0b00000000
		out			TCCR0A, loader
		ldi			workhorse,	0b00000099
		out			TCCR0B,	loader
		ldi			workhorse,	0b00000001
		sts			TIMSK0, loader
		sei

loop:	out			PORTD, amplitude
		rjmp		loop



ISR_OV0:
		inc			amplitude

		reti

